
// SemestralkaDlg.cpp : implementation file
//

#include "stdafx.h"
#include "Semestralka.h"
#include "SemestralkaDlg.h"
#include "afxdialogex.h"
#include <iostream>
#include "boost/filesystem.hpp"
#include <boost/lexical_cast.hpp>
#include <vector>
#include <string>
#include <iostream>
#include <fstream>
#include <boost/crc.hpp>
#include "SettingsDialog.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif
using namespace boost::filesystem;
using namespace std;
const CSemestralkaDlg::CSIDL CSemestralkaDlg::m_nCsidl[] =
{
	{ _T("CSIDL_DESKTOP"),                   0x0000 },  // <desktop>
	{ _T("CSIDL_ADMINTOOLS"),                0x0030 },  // <user name>\Start Menu\Programs\Administrative Tools
	{ _T("CSIDL_ALTSTARTUP"),                0x001d },  // non localized startup
	{ _T("CSIDL_APPDATA"),                   0x001a },  // <user name>\Application Data
	{ _T("CSIDL_BITBUCKET"),                 0x000a },  // <desktop>\Recycle Bin
	{ _T("CSIDL_CDBURN_AREA"),               0x003b },  // USERPROFILE\Local Settings\Application Data\Microsoft\CD Burning
	{ _T("CSIDL_COMMON_ADMINTOOLS"),         0x002f },  // All Users\Start Menu\Programs\Administrative Tools
	{ _T("CSIDL_COMMON_ALTSTARTUP"),         0x001e },  // non localized common startup
	{ _T("CSIDL_COMMON_APPDATA"),            0x0023 },  // All Users\Application Data
	{ _T("CSIDL_COMMON_DESKTOPDIRECTORY"),   0x0019 },  // All Users\Desktop
	{ _T("CSIDL_COMMON_DOCUMENTS"),          0x002e },  // All Users\Documents
	{ _T("CSIDL_COMMON_FAVORITES"),          0x001f },
	{ _T("CSIDL_COMMON_MUSIC"),              0x0035 },  // All Users\My Music
	{ _T("CSIDL_COMMON_OEM_LINKS"),          0x003a },  // Links to All Users OEM specific apps
	{ _T("CSIDL_COMMON_PICTURES"),           0x0036 },  // All Users\My Pictures
	{ _T("CSIDL_COMMON_PROGRAMS"),           0X0017 },  // All Users\Start Menu\Programs
	{ _T("CSIDL_COMMON_STARTMENU"),          0x0016 },  // All Users\Start Menu
	{ _T("CSIDL_COMMON_STARTUP"),            0x0018 },  // All Users\Startup
	{ _T("CSIDL_COMMON_TEMPLATES"),          0x002d },  // All Users\Templates
	{ _T("CSIDL_COMMON_VIDEO"),              0x0037 },  // All Users\My Video
	{ _T("CSIDL_COMPUTERSNEARME"),           0x003d },  // Computers Near Me (computered from Workgroup membership)
	{ _T("CSIDL_CONNECTIONS"),               0x0031 },  // Network and Dial-up Connections
	{ _T("CSIDL_CONTROLS"),                  0x0003 },  // My Computer\Control Panel
	{ _T("CSIDL_COOKIES"),                   0x0021 },
	{ _T("CSIDL_DESKTOPDIRECTORY"),          0x0010 },  // <user name>\Desktop
	{ _T("CSIDL_DRIVES"),                    0x0011 },  // My Computer
	{ _T("CSIDL_FAVORITES"),                 0x0006 },  // <user name>\Favorites
	{ _T("CSIDL_FONTS"),                     0x0014 },  // windows\fonts
	{ _T("CSIDL_HISTORY"),                   0x0022 },
	{ _T("CSIDL_INTERNET"),                  0x0001 },  // Internet Explorer (icon on desktop)
	{ _T("CSIDL_INTERNET_CACHE"),            0x0020 },
	{ _T("CSIDL_LOCAL_APPDATA"),             0x001c },  // <user name>\Local Settings\Applicaiton Data (non roaming)
	{ _T("CSIDL_MYDOCUMENTS"),               0x000c },  // logical "My Documents" desktop icon
	{ _T("CSIDL_MYMUSIC"),                   0x000d },  // "My Music" folder
	{ _T("CSIDL_MYPICTURES"),                0x0027 },  // C:\Program Files\My Pictures
	{ _T("CSIDL_MYVIDEO"),                   0x000e },  // "My Videos" folder
	{ _T("CSIDL_NETHOOD"),                   0x0013 },  // <user name>\nethood
	{ _T("CSIDL_NETWORK"),                   0x0012 },  // Network Neighborhood (My Network Places)
	{ _T("CSIDL_PERSONAL"),                  0x0005 },  // My Documents
	{ _T("CSIDL_PRINTERS"),                  0x0004 },  // My Computer\Printers
	{ _T("CSIDL_PRINTHOOD"),                 0x001b },  // <user name>\PrintHood
	{ _T("CSIDL_PROFILE"),                   0x0028 },  // USERPROFILE
	{ _T("CSIDL_PROGRAMS"),                  0x0002 },  // Start Menu\Programs
	{ _T("CSIDL_PROGRAM_FILES"),             0x0026 },  // C:\Program Files
	{ _T("CSIDL_PROGRAM_FILESX86"),          0x002a },  // x86 C:\Program Files on RISC
	{ _T("CSIDL_PROGRAM_FILES_COMMON"),      0x002b },  // C:\Program Files\Common
	{ _T("CSIDL_PROGRAM_FILES_COMMONX86"),   0x002c },  // x86 Program Files\Common on RISC
	{ _T("CSIDL_RECENT"),                    0x0008 },  // <user name>\Recent
	{ _T("CSIDL_RESOURCES"),                 0x0038 },  // Resource Direcotry
	{ _T("CSIDL_RESOURCES_LOCALIZED"),       0x0039 },  // Localized Resource Direcotry
	{ _T("CSIDL_SENDTO"),                    0x0009 },  // <user name>\SendTo
	{ _T("CSIDL_STARTMENU"),                 0x000b },  // <user name>\Start Menu
	{ _T("CSIDL_STARTUP"),                   0x0007 },  // Start Menu\Programs\Startup
	{ _T("CSIDL_SYSTEM"),                    0x0025 },  // GetSystemDirectory()
	{ _T("CSIDL_SYSTEMX86"),                 0x0029 },  // x86 system directory on RISC
	{ _T("CSIDL_TEMPLATES"),                 0x0015 },
	{ _T("CSIDL_WINDOWS"),                   0x0024 },  // GetWindowsDirectory()
	{ NULL,                                  0 }
};
// CAboutDlg dialog used for App About

boost::system::error_code ec;

std::size_t fileSize(std::ifstream& file)
{
	std::streampos current = file.tellg();
	file.seekg(0, std::ios::end);
	std::size_t result = file.tellg();
	file.seekg(current, std::ios::beg);
	return result;
}

std::uint32_t crc32(const std::string &fp)
{
	std::ifstream is{ fp, std::ios::in | std::ios::binary };
	std::size_t file_size = fileSize(is);
	std::vector<char> buf(file_size);

	boost::crc_32_type result;

	std::vector<char>   buffer(4096);

	while (is.read(&buffer[0], buffer.size()))
	{
		std::size_t count = is.gcount();
		result.process_bytes(&buffer[0], count);
	}

	return result.checksum();
}

class CAboutDlg : public CDialogEx
{
public:
	CAboutDlg();

// Dialog Data
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_ABOUTBOX };
#endif

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

// Implementation
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialogEx(IDD_ABOUTBOX)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialogEx)
END_MESSAGE_MAP()

CSemestralkaDlg::CSemestralkaDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(IDD_SEMESTRALKA_DIALOG, pParent)
	, m_path(_T(""))
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CSemestralkaDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_EDIT1, m_editBoxPath);
	DDX_Text(pDX, IDC_EDIT1, m_path);
	DDX_Control(pDX, IDC_LIST1, m_listItem);
}

BEGIN_MESSAGE_MAP(CSemestralkaDlg, CDialogEx)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_WM_LBUTTONDOWN()
	ON_BN_CLICKED(IDCANCEL, &CSemestralkaDlg::OnBnClickedCancel)
	ON_BN_CLICKED(IDC_BUTTON1, &CSemestralkaDlg::OnBnClickedButton1)
	ON_EN_CHANGE(IDC_EDIT1, &CSemestralkaDlg::OnEnChangeEdit1)
	ON_BN_CLICKED(IDC_BUTTON2, &CSemestralkaDlg::OnBnClickedButton2)
	ON_LBN_SELCHANGE(IDC_LIST1, &CSemestralkaDlg::OnLbnSelchangeList1)
	ON_COMMAND(ID_SETTINGS_SETTINGS, &CSemestralkaDlg::OnSettingsSettings)
	ON_COMMAND(ID_TOOLS_GENERATEEXEFILES, &CSemestralkaDlg::OnRemoveCopies)
	ON_COMMAND(ID_INFO_ABOUTUS, &CSemestralkaDlg::OnInfoAboutus)
END_MESSAGE_MAP()

// CSemestralkaDlg message handlers

BOOL CSemestralkaDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// Add "About..." menu item to system menu.

	// IDM_ABOUTBOX must be in the system command range.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		BOOL bNameValid;
		CString strAboutMenu;
		bNameValid = strAboutMenu.LoadString(IDS_ABOUTBOX);
		ASSERT(bNameValid);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon
	target.insert(".jpg");
	checksum = false;
	// TODO: Add extra initialization here

	return TRUE;  // return TRUE  unless you set the focus to a control
}

void CSemestralkaDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialogEx::OnSysCommand(nID, lParam);
	}
}

void CSemestralkaDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
}

HCURSOR CSemestralkaDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}

void CSemestralkaDlg::OnLButtonDown(UINT nFlags, CPoint point)
{
	// TODO: Add your message handler code here and/or call default
	TRACE("Stlaa�ene tla�itko lavej my�i");
	CDialogEx::OnLButtonDown(nFlags, point);
}

void CSemestralkaDlg::OnBnClickedCancel()
{
	// TODO: Add your control notification handler code here
	CDialogEx::OnCancel();
}

void CSemestralkaDlg::OnBnClickedButton1()
{
	UpdateData(TRUE);
	m_editBoxPath.SetWindowText(_T(""));

	BROWSEINFO bi = { 0 };

	bi.hwndOwner = m_hWnd;
	bi.ulFlags = BIF_RETURNONLYFSDIRS;
	bi.ulFlags |= BIF_NONEWFOLDERBUTTON;
	int nRootFolder = m_nCsidl[m_nRootIndex].nCsidl;
	LPITEMIDLIST pidlRoot = NULL;

	if (SUCCEEDED(SHGetSpecialFolderLocation(m_hWnd, nRootFolder, &pidlRoot)))
		bi.pidlRoot = pidlRoot;

	LPITEMIDLIST pidl = SHBrowseForFolder(&bi);

	BOOL bRet = FALSE;

	TCHAR szFolder[MAX_PATH * 2] = { _T('\0') };

	if (pidl)
	{
		if (SHGetPathFromIDList(pidl, szFolder))
		{
			bRet = TRUE;
		}

		IMalloc *pMalloc = NULL;
		if (SUCCEEDED(SHGetMalloc(&pMalloc)) && pMalloc)
		{
			pMalloc->Free(pidl);
			pMalloc->Release();
		}
	}

	m_editBoxPath.SetWindowText(szFolder);
	folderPath = path(szFolder);
}

void CSemestralkaDlg::OnBnClickedButton2()
{
	if (folderPath != L"") {
		CString str, str2, fin;

		CString crcc, strcompare, crccomparec;
		std::string crc, crccompare;

		m_listItem.ResetContent();
		zoznamduplicities.clear();
		directory_iterator end_itr;

		recursive_directory_iterator dir(folderPath), end;
		while (dir != end)
		{
			string current_file = dir->path().filename().string();
			string current_file_path = dir->path().string();
			string currentExtension = dir->path().extension().string();

			if (is_regular_file(current_file_path) && target.find(currentExtension) != target.end())
			{

				try {
					current_file_size = boost::lexical_cast<std::string>(file_size(dir->path()));
				}
				catch (filesystem_error& e) {
					std::cout << e.what() << '\n';
				}

				zoznam.push_back(current_file);
				zoznamfullpath.push_back(current_file_path);
			}

			++dir;
		}

		if (checksum) {
			for (int i = 0; i < zoznam.size(); i++) {
				str = zoznam[i].c_str();

				for (int k = 0; k < zoznam.size(); k++) {
					strcompare = zoznam[k].c_str();
					
					try {
						crc = std::to_string(crc32(zoznamfullpath[i]));
						crccompare = std::to_string(crc32(zoznamfullpath[k]));

						if (crc == crccompare && (i != k) && std::find(zoznamduplicities.begin(), zoznamduplicities.end(), zoznamfullpath[k].c_str()) == zoznamduplicities.end()) {
							zoznamduplicities.push_back((crc + "?" + zoznamfullpath[k]).c_str());
						}
						
					}
					catch (filesystem_error& e) {
						std::cout << e.what() << '\n';
					}
				}
			}
		}


		else {
			for (int i = 0; i < zoznam.size(); i++) {
				str = zoznam[i].c_str();

				for (int k = 0; k < zoznam.size(); k++) {
					strcompare = zoznam[k].c_str();
					if ((str == strcompare) && (i != k))
					{
						try {
							crc = std::to_string(crc32(zoznamfullpath[i]));
							crccompare = std::to_string(crc32(zoznamfullpath[k]));

							if (crc == crccompare && ((std::find(zoznamduplicities.begin(), zoznamduplicities.end(), zoznamfullpath[k].c_str()) != zoznamduplicities.end()) == false)) {
									zoznamduplicities.push_back((crc+"?"+zoznamfullpath[k]).c_str());
								
							}
						}
						catch (filesystem_error& e) {
							std::cout << e.what() << '\n';
						}
					}
				}
			}
		}

		sort(zoznamduplicities.begin(), zoznamduplicities.end());
		zoznamduplicities.erase(unique(zoznamduplicities.begin(), zoznamduplicities.end()), zoznamduplicities.end());

		if (zoznamduplicities.size() == 0) {
			MessageBox(L"No duplicities found", L"Info", MB_ICONINFORMATION | MB_OK);
		}
		else 
		{
			for (int i = 0; i < zoznamduplicities.size(); i++) {
				fin = zoznamduplicities[i].c_str();
				m_listItem.AddString(fin + " ");
			}
			int size = zoznamduplicities.size();

			CString msg;
			msg.Format(_T("%d copies found"), size);
			MessageBox( msg , L"Info", MB_ICONINFORMATION | MB_OK);
		}	

		zoznam.clear();
	}
	else 
	{
		MessageBox(L"Please choose directory", L"Info", MB_ICONINFORMATION | MB_OK);
	}
}

void CSemestralkaDlg::OnEnChangeEdit1(){
}

void CSemestralkaDlg::OnLbnSelchangeList1()
{
	// TODO: Add your control notification handler code here
}

void CSemestralkaDlg::OnSettingsSettings()
{
	target.clear();
	checksum = false;
	target.insert(".jpg");

	SettingsDialog settingsDialog;
	INT_PTR result = settingsDialog.DoModal();
	if (result == IDOK) {
		
		if (settingsDialog.pngCheckBox) {
			target.insert(".png");
		}

		if (settingsDialog.bmpCheck) {
			target.insert(".bmp");
		}

		if (settingsDialog.gifCheckBox) {
			target.insert(".gif");
		}

		checksum = settingsDialog.checksumCheckBox;
	}

	
}

void CSemestralkaDlg::OnRemoveCopies()
{
	std::string delimiter = "?";
	if (zoznamduplicities.size() != 0)
		for (int i = 1; i < zoznamduplicities.size(); i++) {

			std::string token = zoznamduplicities[i].substr(0, zoznamduplicities[i].find(delimiter));
			std::string tokenMinusOne = zoznamduplicities[i-1].substr(0, zoznamduplicities[i-1].find(delimiter));

			if (token == tokenMinusOne) {
				std::string path = zoznamduplicities[i].substr(zoznamduplicities[i].find("?") + 1);
				CString pathc(path.c_str());
				boost::filesystem::remove(path);
			}
		}
	MessageBox(L"All copies was deleted", L"Info", MB_ICONINFORMATION | MB_OK);
	zoznam.clear();
	zoznamduplicities.clear();
	m_listItem.ResetContent();
}

void CSemestralkaDlg::OnInfoAboutus()
{
	CAboutDlg dlg;
	dlg.DoModal();
}

