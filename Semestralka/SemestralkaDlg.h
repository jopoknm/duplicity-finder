
// SemestralkaDlg.h : header file
//

#pragma once
#include "afxwin.h"
#include <vector>
#include <string>
#include <iostream>
#include <fstream>
#include <boost/crc.hpp>
#include "boost/filesystem.hpp"
#include <boost/lexical_cast.hpp>
#include <set>
#include "DuplicateFiles.h"
using namespace boost::filesystem;
// CSemestralkaDlg dialog
class CSemestralkaDlg : public CDialogEx
{
// Construction
public:
	CSemestralkaDlg(CWnd* pParent = NULL);	// standard constructor

// Dialog Data
//#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_SEMESTRALKA_DIALOG };
	BOOL	m_bEditBox;

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support
	int	m_nRootIndex;

// Implementation
protected:
	HICON m_hIcon;

	// Generated message map functions
	virtual BOOL OnInitDialog();
	
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);;
	afx_msg void OnBnClickedCancel();
	afx_msg void OnBnClickedButton1();
	afx_msg void OnEnChangeEdit1();

	CEdit m_editBoxPath;
	CString m_path;

	std::vector<std::string> zoznam;
	std::vector<std::string> zoznamfullpath;
	std::vector<std::string> zoznamfilesize;
	std::vector<std::string> zoznamduplicities;
	std::set<std::string> target;
	std::string current_file_size;

	struct CSIDL
	{
		TCHAR * pszCsidl;
		int		nCsidl;
	};

	path folderPath;
	static const CSIDL m_nCsidl[];
	CListBox m_listItem;
	afx_msg void OnBnClickedButton2();
	afx_msg void OnLbnSelchangeList1();
	afx_msg void OnSettingsSettings();

	BOOL checksum;

	afx_msg void OnRemoveCopies();
	afx_msg void OnInfoAboutus();
};
