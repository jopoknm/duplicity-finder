#pragma once
#include <string>

class DuplicateFiles
{
	
public:
	DuplicateFiles(std::string path, std:: string crc);
	~DuplicateFiles();
	std::string GetPath();
	std::string GetCrc();
	std::string path;
	std::string crc;
private:

};

