#pragma once
#include "afxwin.h"


// SettingsDialog dialog

class SettingsDialog : public CDialogEx
{
	DECLARE_DYNAMIC(SettingsDialog)

public:
	SettingsDialog(CWnd* pParent = NULL);   // standard constructor
	virtual ~SettingsDialog();

// Dialog Data
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_SETTING_DIALOG };
#endif

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL OnInitDialog();
	DECLARE_MESSAGE_MAP()
public:
	CButton jpgCheckBox;
	BOOL pngCheckBox;
	BOOL bmpCheck;
	BOOL gifCheckBox;
	CButton fileNameCheckBox;
	BOOL checksumCheckBox;
};
