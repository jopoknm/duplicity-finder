#include "stdafx.h"
#include "DuplicateFiles.h"


DuplicateFiles::DuplicateFiles(std::string pat, std::string c)
{
	path = pat;
	crc = c;
}


DuplicateFiles::~DuplicateFiles()
{
}

std::string DuplicateFiles::GetPath()
{
	return path;
}

std::string DuplicateFiles::GetCrc()
{
	return crc;
}


