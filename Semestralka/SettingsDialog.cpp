// SettingsDialog.cpp : implementation file
//

#include "stdafx.h"
#include "Semestralka.h"
#include "SettingsDialog.h"
#include "afxdialogex.h"


// SettingsDialog dialog

IMPLEMENT_DYNAMIC(SettingsDialog, CDialogEx)

SettingsDialog::SettingsDialog(CWnd* pParent /*=NULL*/)
	: CDialogEx(IDD_SETTING_DIALOG, pParent)
	, pngCheckBox(FALSE)
	, bmpCheck(FALSE)
	, gifCheckBox(FALSE)
	, checksumCheckBox(FALSE)
{

}

SettingsDialog::~SettingsDialog()
{
}

void SettingsDialog::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_CHECK_JPG, jpgCheckBox);
	DDX_Check(pDX, IDC_CHECK_PNG, pngCheckBox);
	DDX_Check(pDX, IDC_CHECK_BMP, bmpCheck);
	DDX_Check(pDX, IDC_CHECK_GIF, gifCheckBox);
	DDX_Control(pDX, IDC_CHECK5, fileNameCheckBox);
	DDX_Check(pDX, IDC_CHECK7, checksumCheckBox);
}


BEGIN_MESSAGE_MAP(SettingsDialog, CDialogEx)
END_MESSAGE_MAP()

BOOL SettingsDialog::OnInitDialog()
{
	BOOL res = CDialogEx::OnInitDialog();

	jpgCheckBox.SetCheck(true);
	jpgCheckBox.EnableWindow(false);

	fileNameCheckBox.SetCheck(true);
	fileNameCheckBox.EnableWindow(false);

	return res;  
}
// SettingsDialog message handlers
